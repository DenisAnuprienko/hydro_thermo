#include <iostream>
#include <string>
#include <fstream>

//     We work with N x N grid
//     Upper and lower boundaries have Dirichlet BC
//     Side boundaries have Neumann BC

//     Schematic representation of unknowns on cell (i,j) for fluid flow

//     +---------qy(i,j+1)------+
//     |                        |
//     |                        |
//     |                        |
//     |                        |
//     qx(i,j)    P(i,j)       qx(i+1,j)
//     |                        |
//     |                        |
//     |                        |
//     |                        |
//     +---------qy(i,j)--------+
using namespace std;

// Domain and mesh parameters
const double   L      = 1.0;      // Square side size
const unsigned Ncells = 32;       // Number of points in one dimension
const double   h =    L / Ncells; // Mesh step size

// Physical parameters
const double beta_f = 1.0; // Fluid compressibilty
const double K      = 1.0; // Medium permeability
const double eta_f  = 1.0; // Fluid viscocity
const double rho_f0 = 1.0; // Fluid density
const double g      = 9.81;
const double lambda = 1.0; // Heat conductivity
const double rho    = 1.0; // Density
const double c_p    = 1.0; // Heat capacity

double GetDensity(double T)
{
    return rho_f0 * (1.0 - 8e-1 * T);
}

// BC functions
double LowerTemp(double x);
double UpperTemp(double x);
double UpperHead(double x);
double LowerHead(double x);


// For variables located at cell centers
class CellVar{
private:
    unsigned size;
    double *vals;

public:
    CellVar(unsigned num) { size = num; vals = new double[size*size];}
    ~CellVar() { delete [] vals; }
    double & operator()(unsigned i, unsigned j)
    {
        return vals[j * size + i];
    }
};

// For variables located at vertical faces, i.e. qx
class XFaceVar{
private:
    unsigned size;
    double *vals;

public:
    XFaceVar(unsigned num) { size = num; vals = new double[size*(size+1)];}
    ~XFaceVar() { delete [] vals; }
    double & operator()(unsigned i, unsigned j)
    {
        return vals[j * size + i];
    }
};

// For variables located at horizontal faces, i.e. qy
class YFaceVar{
private:
    unsigned size;
    double *vals;

public:
    YFaceVar(unsigned num) { size = num; vals = new double[size*(size+1)];}
    ~YFaceVar() { delete [] vals; }
    double & operator()(unsigned i, unsigned j)
    {
        return vals[j * (size-1) + i];
    }
};

class VertexVar
{
private:
    unsigned size;
    double *vals;
public:
    VertexVar(unsigned _N) { size = _N+1; vals = new double[size * size]; }
    ~VertexVar() { delete [] vals; }
    double & operator()(unsigned i, unsigned j)
    {
        return vals[j * size + i];
    }
};

class Problem
{
private:
    unsigned N;  // Number of mesh cells in one dimension
    double   dt; // Time step size
    CellVar  P;  // Fluid pressure
    XFaceVar qx; // Fluid flux in x-direction
    YFaceVar qy; // Fluid flux in y-direction

    CellVar  T;  // Fluid temperature
    CellVar  tau_xx, tau_yy;
    VertexVar tau_xy;

public:
    Problem(unsigned _N, double deltat) :
        P(_N),
        qx(_N), qy(_N),
        T(_N),
        tau_xx(_N), tau_yy(_N), tau_xy(_N)
    {
        N = _N;
        dt = deltat;
    }
    ~Problem() {}
    void Init(void);
    void ComputeFluidFluxes(void);
    void MakeHydroStep(void);
    void MakeViscousFlowStep(void);
    void MakeThermoStep(void);
    void SaveVTK(string path);
};

void Problem::Init(void)
{
    // Cell variables
    for(unsigned i = 0; i < N; i++){
        for(unsigned j = 0; j < N; j++){
            P(i,j)      = 0.0;
            T(i,j)      = 0.0;
            tau_xx(i,j) = 0.0;
            tau_yy(i,j) = 0.0;
        }
    }

    // X-face variables
    for(unsigned i = 0; i <= N; i++){
        for(unsigned j = 0; j < N; j++){
            qx(i,j) = 0.0;
        }
    }
    // Y-face variables
    for(unsigned i = 0; i < N; i++){
        for(unsigned j = 0; j <= N; j++){
            qy(i,j) = 0.0;
        }
    }
    // Vertex variables
    for(unsigned i = 0; i <= N; i++){
        for(unsigned j = 0; j <= N; j++){
            tau_xy(i,j) = 0.0;
        }
    }
}

void Problem::ComputeFluidFluxes(void)
{
    // Calculation of X-flux
    for(unsigned i = 0; i <= N; i++){
        for(unsigned j = 0; j < N; j++){
            // Flux in X-direction
            if(i == 0 || i == N){
                qx(i, j) = 0.0; // Zero-flux BC at sides
            }
            else{
                qx(i, j) = -K / eta_f * (P(i,j) - P(i-1,j)) / h;
            }
            if(std::isinf(qx(i,j)) || std::isnan(qx(i,j))){
                cout << "Bad qx at cell (" << i << ", " << j << ")" << endl;
                exit(0);
            }
        }
    }

    // Calculation of Y-flux
    for(unsigned i = 0; i < N; i++){
        for(unsigned j = 0; j <= N; j++){

            double rho_f;
            if(j == N)
                rho_f = GetDensity(UpperTemp(0));
            else
                rho_f = GetDensity(T(i,j));
            //if(rh)

            // Flux in Y-direction
            if(j == N){
                qy(i, j) = -K / eta_f * ((UpperHead(0) - P(i,j-1)) / (0.5*h) + rho_f*g);
            }
            else if(j == 0){
                qy(i, j) = -K / eta_f * ((P(i,j) - LowerHead(0)) / (0.5*h) + rho_f*g);
            }
            else{
                qy(i, j) = -K / eta_f * ((P(i,j) - P(i,j-1)) / h + rho_f*g);
            }

            if(std::isinf(qy(i,j)) || std::isnan(qy(i,j)) || fabs(qy(i,j)) > 1e20){
                cout << "Bad qy = " << qy(i,j) << " at cell (" << i << ", " << j << ")" << endl;
                cout << "P = " << P(i,j) << " " << P(i, j-1) << endl;
                cout << "dP/dy = " << (P(i,j) - P(i, j-1))/h << endl;
                cout << "T = " << T(i,j) << endl;
                exit(0);
            }
        }
    }
}

void Problem::MakeHydroStep(void)
{
    for(unsigned i = 0; i < N; i++){
        for(unsigned j = 0; j < N; j++){
            // We are at cell (i,j)
            double dqx_dx = (qx(i+1,j) - qx(i,j)) / h;
            double dqy_dy = (qy(i,j+1) - qy(i,j)) / h;
            P(i,j) -= (dqx_dx + dqy_dy) * dt / beta_f;
            if(std::isinf(P(i,j)) || std::isnan(P(i,j)) || fabs(P(i,j)) > 1e20){
                cout << "Bad P at cell (" << i << ", " << j << ")" << endl;
                cout << "qy = " << qy(i,j) << " " << qy(i, j+1) << endl;
                exit(0);
            }
        }
    }
    ComputeFluidFluxes();
}

void Problem::MakeViscousFlowStep(void)
{
    // Pressure update
    for(unsigned i = 0; i < N; i++){
        for(unsigned j = 0; j < N; j++){
            // We are at cell (i,j)
            double dqx_dx = (qx(i+1,j) - qx(i,j)) / h;
            double dqy_dy = (qy(i,j+1) - qy(i,j)) / h;
            P(i,j) -= (dqx_dx + dqy_dy) * dt / beta_f;
        }
    }

    // X-Flux update
    for(unsigned i = 0; i <= N; i++){
        for(unsigned j = 0; j < N; j++){
            if(i == 0 || i == N)
                qx(i,j) = 0.0;
            else{
                double dP_dx = (P(i,j) - P(i-1,j)) / h;
                double dtau_xx_dx = (tau_xx(i,j) - tau_xx(i-1,j)) / h;
                double dtau_xy_dy = (tau_xy(i,j+1) - tau_xy(i,j)) / h;
                double update = -dP_dx + dtau_xx_dx + dtau_xy_dy;
                qx(i,j) += update * dt / rho;
            }
        }
    }
    // Y-Flux update
    for(unsigned i = 0; i < N; i++){
        for(unsigned j = 0; j <= N; j++){
            if(j == 0){
                double dP_dy = (P(i,j) - LowerHead(0)) / (0.5*h);
                double dtau_yy_dy = (tau_yy(i,j) - 0.0) / (0.5*h);
                double dtau_xy_dx = (tau_xy(i+1,j) - tau_xy(i,j)) / h;
                double grav_term = rho_f0 * g;
                double update = -dP_dy + dtau_yy_dy + dtau_xy_dx + grav_term;
                qy(i,j) += update * dt / rho;
            }
            else if(j == N){
                double dP_dy = (UpperHead(0) - P(i,j-1)) / (0.5*h);
                double dtau_yy_dy = (0.0 - tau_yy(i,j-1)) / (0.5*h);
                double dtau_xy_dx = (tau_xy(i+1,j) - tau_xy(i,j)) / h;
                double grav_term = rho_f0 * g;
                double update = -dP_dy + dtau_yy_dy + dtau_xy_dx + grav_term;
                qy(i,j) += update * dt / rho;
            }
            else{
                double dP_dy = (P(i,j) - P(i,j-1)) / h;
                double dtau_yy_dy = (tau_yy(i,j) - tau_yy(i,j-1)) / h;
                double dtau_xy_dx = (tau_xy(i+1,j) - tau_xy(i,j)) / h;
                double grav_term = rho_f0 * g;
                double update = -dP_dy + dtau_yy_dy + dtau_xy_dx + grav_term;
                qy(i,j) += update * dt / rho;
            }
        }
    }

    // tau_xx and tau_yy update
    for(unsigned i = 0; i < N; i++){
        for(unsigned j = 0; j < N; j++){
            // Cell (i,j)
            double dqx_dx = (qx(i+1,j) - qx(i,j)) / h;
            double dqy_dy = (qy(i,j+1) - qy(i,j)) / h;
            tau_xx(i,j) = 2.0 * eta_f * (2.0/3.0 * dqx_dx
                                        -1.0/3.0 * dqy_dy);
            tau_yy(i,j) = 2.0 * eta_f * (2.0/3.0 * dqy_dy
                                        -1.0/3.0 * dqx_dx);
        }
    }
    // tau_xy update
    for(unsigned i = 0; i <= N; i++){
        for(unsigned j = 0; j <= N; j++){
            if(i == 0 || i == N || j == 0 || j == N){
                tau_xy(i,j) = 0.0; // Boundary
            }
            else{
                double dqx_dy = (qx(i,j) - qx(i,j-1)) / h;
                double dqy_dx = (qy(i,j) - qy(i-1,j)) / h;
                tau_xy(i,j) = eta_f * (dqx_dy + dqy_dx);
            }
        }
    }
}

void Problem::MakeThermoStep(void)
{
    for(unsigned i = 0; i < N; i++){
        for(unsigned j = 0; j < N; j++){
            // We are at cell (i,j)

            double update = 0.0;

            // Convection part
            double dT_dx = 0.0, dT_dy = 0.0;
            // Use fluid flux at left and lower edges to determine upwind values
            if(qx(i,j) > 0.0){
                if(i == 0){
                    dT_dx = 0.0;// No flow at left boundary
                }
                else{
                    dT_dx = (T(i,j) - T(i-1,j)) / h;
                }
            }
            else{
                if(i == N-1){
                    dT_dx = 0.0;
                }
                else{
                    dT_dx = (T(i+1,j) - T(i,j)) / h;
                }
            }
            if(qy(i,j) > 0){
                if(j == 0){
                    dT_dy = (T(i,j) - LowerTemp(0)) / (0.5*h);
                }
                else{
                    dT_dy = (T(i,j) - T(i-1,j)) / h;
                }
            }
            else{
                if(j == N-1){
                    dT_dy = (UpperTemp(0) - T(i,j)) / (0.5*h);
                }
                else{
                    dT_dy = (T(i,j+1) - T(i,j)) / h;
                }
            }

            update = -rho * c_p * (qx(i,j) * dT_dx + qy(i,j) * dT_dy);

            // Heat diffusion part
//            double T_upr, T_lwr, T_lft, T_rgt;
//            if(i > 0)
//                T_lwr = T(i,j-1);
//            else
//                T_lwr =


            T(i,j) += update * dt;
        }
    }
}

int main(void)
{
    cout << "h = " << h << endl;
    Problem problem(Ncells, 1e-6);
    problem.Init();
    problem.ComputeFluidFluxes();
    const string s = "C:\\temp\\res\\test.vtk";
    problem.SaveVTK("C:\\temp\\res\\sol0.vtk");

    int nsol = 1;
    for(int tstep = 0; tstep < 2e6; tstep++){
        //problem.MakeHydroStep();
        problem.MakeViscousFlowStep();
        //problem.MakeThermoStep();
        if(tstep%1000 == 0){
            cout << "Time step " << tstep << endl;
        }
        if(tstep%10000 == 0){
            cout << "Saving sol " << nsol << endl;
            string str = "C:\\temp\\res\\sol";
            str += to_string(nsol);
            str += ".vtk";
            problem.SaveVTK(str);
            nsol++;
        }
    }

    return 0;
}


void Problem::SaveVTK(string path)
{
    ofstream out;
    out.open(path);
    out << "# vtk DataFile Version 3.0" << endl << endl;
    out << "ASCII" << endl;
    out << "DATASET STRUCTURED_GRID" << endl;
    out << "DIMENSIONS " << N+1 << " " << N+1 << " 1" << endl;
    out << "POINTS " << (N+1)*(N+1) << " DOUBLE" << endl;

    for(unsigned i = 0; i <= N; i++){
        for(unsigned j = 0; j <= N; j++){
            out << j*h << " " << i*h << " 0.0" << endl;
        }
    }
//    out << "DATASET RECTILINEAR_GRID" << endl;

//    out << "DIMENSIONS " << N+1 << " " << N+1 << " 1" << endl;
//    out << "X_COORDINATES " << N+1 << " double" << endl;
//    for(unsigned i = 0; i <= N; i++)
//        out << i * h << endl;
//    out << "Y_COORDINATES " << N+1 << " double" << endl;
//    for(unsigned i = 0; i <= N; i++)
//        out << i * h << endl;
//    out << "Z_COORDINATES " << 1 << endl << 0 << endl;

    out << "CELL_DATA " << N * N << endl;

    out << "SCALARS Pressure double" << endl;
    out << "LOOKUP_TABLE default" << endl;
    for(unsigned j = 0; j < N; j++){
        for(unsigned i = 0; i < N; i++){
            out << P(i, j) << endl;
        }
    }

    out << "SCALARS Temperature double" << endl;
    out << "LOOKUP_TABLE default" << endl;
    for(unsigned j = 0; j < N; j++){
        for(unsigned i = 0; i < N; i++){
            out << T(i, j) << endl;
        }
    }

    out << "SCALARS FluidDensity double" << endl;
    out << "LOOKUP_TABLE default" << endl;
    for(unsigned j = 0; j < N; j++){
        for(unsigned i = 0; i < N; i++){
            out << GetDensity(T(i,j)) << endl;
        }
    }


    out << "VECTORS FluidFlux double" << endl;
    for(unsigned j = 0; j < N; j++){
        for(unsigned i = 0; i < N; i++){
            out << qx(i, j) << " " << qy(i, j) << " 0.0" << endl;
        }
    }

    out.close();
}

double LowerTemp(double x)
{
    return 0.0;
}

double UpperTemp(double x)
{
    return 1.0;
}

double UpperHead(double x)
{
    return 1.0;
}
double LowerHead(double x)
{
    return 0.0;
}


